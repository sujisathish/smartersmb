var sql = require('mssql'); 
 
var config = {
    user: 'demouser',
    password: 'demouser',
    server: 'localhost', // You can use 'localhost\\instance' to connect to named instance 
    database: 'smartersmb',
    
    options: {
        encrypt: true // Use this if you're on Windows Azure 
    }
}

var conn = new sql.Connection(config, function(err) { if (err) throw err; });
sql.globalConnection = conn;