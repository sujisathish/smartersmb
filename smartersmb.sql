USE [master]
GO
/****** Object:  Database [smartersmb]    Script Date: 10/14/2015 5:34:39 PM ******/
CREATE DATABASE [smartersmb]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'smartersmb', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\smartersmb.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'smartersmb_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\smartersmb_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [smartersmb] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [smartersmb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [smartersmb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [smartersmb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [smartersmb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [smartersmb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [smartersmb] SET ARITHABORT OFF 
GO
ALTER DATABASE [smartersmb] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [smartersmb] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [smartersmb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [smartersmb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [smartersmb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [smartersmb] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [smartersmb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [smartersmb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [smartersmb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [smartersmb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [smartersmb] SET  DISABLE_BROKER 
GO
ALTER DATABASE [smartersmb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [smartersmb] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [smartersmb] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [smartersmb] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [smartersmb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [smartersmb] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [smartersmb] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [smartersmb] SET RECOVERY FULL 
GO
ALTER DATABASE [smartersmb] SET  MULTI_USER 
GO
ALTER DATABASE [smartersmb] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [smartersmb] SET DB_CHAINING OFF 
GO
ALTER DATABASE [smartersmb] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [smartersmb] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [smartersmb]
GO
/****** Object:  Table [dbo].[business_catalog]    Script Date: 10/14/2015 5:34:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[business_catalog](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[primary_contact] [varchar](13) NULL,
	[company_logo] [varchar](25) NULL,
	[company_name] [varchar](50) NULL,
	[tag_line] [varchar](50) NULL,
	[promotional_video] [varchar](500) NULL,
	[facility_photo1] [varchar](50) NULL,
	[facility_photo2] [varchar](50) NULL,
	[facility_photo3] [varchar](50) NULL,
	[facility_photo4] [varchar](50) NULL,
	[website] [varchar](50) NULL,
	[address] [varchar](100) NULL,
	[audio_testmonial] [varchar](500) NULL,
 CONSTRAINT [PK_business_catalog_table] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[business_catalog] ON 

INSERT [dbo].[business_catalog] ([id], [primary_contact], [company_logo], [company_name], [tag_line], [promotional_video], [facility_photo1], [facility_photo2], [facility_photo3], [facility_photo4], [website], [address], [audio_testmonial]) VALUES (1, N'985632145', N'1444820731910.png', N'Anna University', N'Anna University', N'https://www.youtube.com/embed/62MqJIUQQBw', N'1444820731916.jpg', N'1444820731917.jpg', N'1444820731919.jpg', N'1444820731929.jpg', N'https://www.annauniv.edu/', N'Sardar Patel Road, Guindy, Chennai, Tamil Nadu 600025', N'1444820440286.mp3')
SET IDENTITY_INSERT [dbo].[business_catalog] OFF
USE [master]
GO
ALTER DATABASE [smartersmb] SET  READ_WRITE 
GO
