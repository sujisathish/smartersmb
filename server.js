var express=require('express');
var app=express();
var path = require('path');
var bodyParser = require('body-parser')
var multer  = require('multer');
var expressSession = require('express-session');
var cookieParser = require('cookie-parser'); // the session is stored in a cookie, so we use this to parse it

var routes = require('./router/index');
var user_page = require('./router/user_page.js');



app.use(express.static(path.join(__dirname, 'public')));


app.use(cookieParser());

app.use(expressSession({secret:'abc'}));


app.use(bodyParser.urlencoded({ extended: false }));
//app.use(express.static(__dirname + 'public'));

app.set('views',__dirname + '/views');
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

app.get('/', function(req,res){
	res.render('pages/index')
});



app.use('/user_page', express.static('public'));

app.post('/',routes);
app.use('/user_page',user_page);


var server=app.listen(3000,function(){
console.log("Express is running on port 3000");
});

module.exports = app;

